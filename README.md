# OpenML dataset: thoracic_surgery

https://www.openml.org/d/4329

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data was collected retrospectively at Wroclaw Thoracic Surgery Centre for patients who underwent major lung resections for primary lung cancer in the years 2007 - 2011. The Centre is associated with the Department of Thoracic Dataset from UCI Machine Learning Repository. Surgery of the Medical University of Wroclaw and Lower-Silesian Centre for Pulmonary Diseases, Poland, while the research database constitutes a part of the National Lung Cancer Registry, administered by the Institute of Tuberculosis and Pulmonary Diseases in Warsaw, Poland.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4329) of an [OpenML dataset](https://www.openml.org/d/4329). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4329/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4329/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4329/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

